//
//  CoordinatorFactory.swift
//  mandiri-online-test
//
//  Created by Hedy on 21/09/20.
//

import Foundation
import UIKit

protocol CoordinatorFactory {
    // MARK: - Main
    func makeMainCoordinator(router: Router) -> Coordinator & MainCoordinatorOutput
}
