//
//  RepositoryModuleFactory.swift
//  mandiri-online-test
//
//  Created by Hedy on 21/09/20.
//

import Foundation

protocol RepositoryModuleFactory {
    // MARK: - Main
    func makeListCategoryRepo() -> ListCategoryRepository
    func makeListArticlesRepo() -> ListArticleRepository
}
