//
//  RepositoryModuleFactoryImpl.swift
//  mandiri-online-test
//
//  Created by Hedy on 21/09/20.
//

import Foundation

extension ModuleFactoryImpl: RepositoryModuleFactory {
    
    func makeListCategoryRepo() -> ListCategoryRepository {
        return ListCategoryRepositoryImpl(sourceAPI: makeSourceAPI())
    }
    
    func makeListArticlesRepo() -> ListArticleRepository {
        return ListArticleRepositoryImpl(headlineAPI: makeHeadlineAPI())
    }
}
