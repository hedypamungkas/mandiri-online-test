//
//  VMModuleFactory.swift
//  mandiri-online-test
//
//  Created by Hedy on 21/09/20.
//

import Foundation

protocol VMModuleFactory {
    // MARK: - Main
    func makeListCategoryVM() -> ListCategoryVM
    func makeListSourceVM() -> ListSourceVM
    func makeListArticleVM() -> ListArticleVM
}
