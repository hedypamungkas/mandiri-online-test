//
//  VMModuleFactoryImpl.swift
//  mandiri-online-test
//
//  Created by Hedy on 21/09/20.
//

import Foundation

extension ModuleFactoryImpl: VMModuleFactory {
    
    func makeListCategoryVM() -> ListCategoryVM {
        return ListCategoryVM(repository: makeListCategoryRepo())
    }
    
    func makeListSourceVM() -> ListSourceVM {
        return ListSourceVM()
    }
    
    func makeListArticleVM() -> ListArticleVM {
        return ListArticleVM(repository: makeListArticlesRepo())
    }
    
}
