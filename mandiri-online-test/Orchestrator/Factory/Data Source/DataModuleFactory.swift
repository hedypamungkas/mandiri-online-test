//
//  DataModuleFactory.swift
//  mandiri-online-test
//
//  Created by Hedy on 21/09/20.
//

import Foundation

protocol DataModuleFactory {
    // MARK: - HTTP Client
    func makeBaseIdentifier() -> HTTPIdentifier
    func makeHTTPClient() -> HTTPClient
    
    // MARK: - API
    func makeSourceAPI() -> SourceAPI
    func makeHeadlineAPI() -> HeadlineAPI
}
