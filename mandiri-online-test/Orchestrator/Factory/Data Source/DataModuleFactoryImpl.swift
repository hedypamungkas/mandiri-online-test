//
//  DataModuleFactoryImpl.swift
//  mandiri-online-test
//
//  Created by Hedy on 21/09/20.
//

import Foundation

class ModuleFactoryImpl: DataModuleFactory {
    
    func makeBaseIdentifier() -> HTTPIdentifier {
        return BaseIdentifier()
    }
    
    func makeHTTPClient() -> HTTPClient {
        return HTTPClientImpl(identifier: makeBaseIdentifier())
    }
    
    func makeSourceAPI() -> SourceAPI {
        return SourceAPIImpl(httpClient: makeHTTPClient())
    }
    
    func makeHeadlineAPI() -> HeadlineAPI {
        HeadlineAPIImpl(httpClient: makeHTTPClient())
    }
    
}
