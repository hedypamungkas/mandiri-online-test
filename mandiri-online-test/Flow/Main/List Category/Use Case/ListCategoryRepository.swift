//
//  ListCategoryRepository.swift
//  mandiri-online-test
//
//  Created by Hedy on 21/09/20.
//

import Foundation
import RxSwift

protocol ListCategoryRepository {
    func requestSource() -> Single<([SourceResponseDetail], [String])>
}
