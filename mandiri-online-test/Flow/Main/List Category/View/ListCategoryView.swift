//
//  ListCategoryView.swift
//  mandiri-online-test
//
//  Created by Hedy on 21/09/20.
//

import Foundation

protocol ListCategoryView: BaseView {
    var viewModel: ListCategoryVM! { get set }
    var onCategoryTapped: (([SourceResponseDetail]) -> Void)? { get set }
}

protocol ListCategoryViewInput: AnyObject {
    func onFetchCategories(categories: [String])
    func onViewState(_ state: BasicUIState)
}

protocol ListCategoryViewOutput: AnyObject {
    func viewDidLoad()
    func onCategoryTapped(category: String)
}
