//
//  ListSourceView.swift
//  mandiri-online-test
//
//  Created by Hedy on 05/11/20.
//

import Foundation

protocol ListSourceView: BaseView {
    var viewModel: ListSourceVM! { get set }
    var model: [SourceResponseDetail]! { get set }
    var onSourceTapped: ((String) -> Void)? { get set }
}

protocol ListSourceViewInput: AnyObject {
    func onFetchSources(sources: [SourceResponseDetail])
}

protocol ListSourceViewOutput: AnyObject {
    func viewDidLoad()
    func onSourceTapped(sourceId: String)
    func onSearchSources(keyword: String)
}
