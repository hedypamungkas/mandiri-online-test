//
//  ArticleView.swift
//  mandiri-online-test
//
//  Created by TMLI IT Dev on 05/11/20.
//

import Foundation

protocol ArticleView: BaseView {
    var model: WebviewResourceType! { get set }
}

protocol DetailArticleViewInput: AnyObject {
    func onWebview(resource: WebviewResourceType)
}

protocol DetailArticleViewOutput: AnyObject {
    func viewDidLoad()
    func onDoneTapped()
}
