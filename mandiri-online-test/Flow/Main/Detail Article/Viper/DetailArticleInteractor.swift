//
//  DetailArticleInteractor.swift
//  mandiri-online-test
//
//  Created by TMLI IT Dev on 18/11/20.
//

import Foundation

protocol DetailArticleInteractorOutput: AnyObject {
    
}

final class DetailArticleInteractor: Interactorable {
    
    let resource: WebviewResourceType
    weak var presenter: DetailArticleInteractorOutput?
    
    init(resource: WebviewResourceType) {
        self.resource = resource
    }
}
