//
//  ListArticleView.swift
//  mandiri-online-test
//
//  Created by TMLI IT Dev on 05/11/20.
//

import Foundation

protocol ListArticleView: BaseView {
    var viewModel: ListArticleVM! { get set }
    var model: String! { get set }
    var onArticleTapped: ((WebviewResourceType) -> Void)? { get set }
}

protocol ListArticleViewInput: AnyObject {
    func onFetchArticles(articles: [HeadlineResponseDetail])
    func onViewState(_ state: BasicUIState)
}

protocol ListArticleViewOutput: AnyObject {
    func viewDidLoad()
    func onArticleTapped(resource: WebviewResourceType)
    func onSearchArticles(keyword: String)
}
