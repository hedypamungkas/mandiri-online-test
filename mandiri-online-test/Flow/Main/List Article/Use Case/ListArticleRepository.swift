//
//  ListArticleRepository.swift
//  mandiri-online-test
//
//  Created by TMLI IT Dev on 05/11/20.
//

import Foundation
import RxSwift

protocol ListArticleRepository {
    func requestArticle(keyword: String?, sources: String) -> Single<[HeadlineResponseDetail]>
}
