//
//  Coordinator.swift
//  mandiri-online-test
//
//  Created by Hedy on 21/09/20.
//

import Foundation

protocol Coordinator: class {
    func start()
    func start(with option: DeepLinkOption?)
}
