//
//  Presentable.swift
//  mandiri-online-test
//
//  Created by Hedy on 21/09/20.
//

import Foundation
import UIKit

protocol Presentable {
    func toPresent() -> UIViewController?
}

extension UIViewController: Presentable {
    
    func toPresent() -> UIViewController? {
        return self
    }
}
